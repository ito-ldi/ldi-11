/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package registros;

/**
 *
 * @author Luis
 */
public class TipoByte{
   private String nombre; 
   private String valor;
   private String tipo;
   
   public TipoByte(String nombre, int valor){
       if(comprobarValor(valor+"")){
           this.nombre = nombre;
           this.valor = convercionDecimalABinario(valor+"");
           this.tipo = "Byte";
       }else{
           System.out.println("El valor debe ser menor a 256");
       }
       
   }
   private boolean comprobarValor(String valor){
       return convercionDecimalABinario(valor).length() <= 8;
   }

    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public String getValor(){
        return valor;
    }   
    public String getTipo() {
        return tipo;
    }
    
   
     private String convercionDecimalABinario(String numDecimal){
            String resultado = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numBinario = new String[100];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 2;                
                          decimal = decimal /2;                
                    numBinario[x] = residuo+"";
                    x++;
                                  
              }while(decimal > 0);               
             for(int y = numBinario.length-1; y >= 0; y--){            
                if(!(numBinario[y] == null)){
                  resultado += numBinario[y];
                }
        }     
        return resultado;
    }   
}
