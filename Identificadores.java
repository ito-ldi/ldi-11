/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package registros;

import java.util.ArrayList;

/**
 *
 * @author Luis
 */
public class Identificadores{ 
    private  ArrayList<TipoByte> variByte = new ArrayList<>();
    private  ArrayList<TipoWord> variWord = new ArrayList<>();
    public void agregarIdentificadorBYTE(TipoByte tipoByte){      
        variByte.add(tipoByte);
    }
    public void agregarIdentificadorWORD(TipoWord tipoWord){
       variWord.add(tipoWord);
    }
    
  
    public boolean buscarVariable(String nomVariable){      
        for(TipoByte var: variByte){            
            if(nomVariable.equals(var.getNombre())){                
                return true;
        }
        }
        for(TipoWord vari: variWord){
            if(nomVariable.equals(vari.getNombre())){
                return true;
        }            
        }
        return false;
    }
    public String obtenVariable(String nomVar){
        for(TipoByte var: variByte){
            if(nomVar.equals(var.getNombre())){
                return var.getValor();
            }
        }
         for(TipoWord vari: variWord){
            if(nomVar.equals(vari.getNombre())){
                return vari.getValor();
            }
        }
        return "";
    }
    
     
    
    public void mostrarVariable(){
        System.out.println("\n\tNombre Variable \tTipo Variable \tValor");
        variableByte();
        variableWord();
    }
    private void variableByte(){
        for(TipoByte vari : variByte){
             System.out.print("\n\t\t"+vari.getNombre()+"\t\t"+vari.getTipo()+"\t\t"+vari.getValor());
        }
        System.out.println("");
    }
    private void variableWord(){
        for(TipoWord vari : variWord){
             System.out.print("\n\t\t"+vari.getNombre()+"\t\t"+vari.getTipo()+"\t\t"+vari.getValor());
        }
        System.out.println("");
    }
    
        
    }
    

