/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package registros;

/**
 *
 * @author Luis
 */
public class Registros{
    private final String[] regisAX;
    private final String[] regisBX;
    private final String[] regisCX;
    private final String[] regisDX;
    public static Identificadores uno = new Identificadores();
    
    private static final int bitSuperiorInicio = 1;
    private static final int bitSuperiorfin    = 8;
    private static final int bitInferiorInicio = 9;
    private static final int bitInferiorfin    = 16;
    private static final int bitInicio         = 1;
    private static final int bitFinal          = 16;
    
    public Registros(){
        regisAX = new String[17];
        regisBX = new String[17];
        regisCX = new String[17];
        regisDX = new String[17];
        inicializarRegistros();
    }
    private void inicializarRegistros(){
           regisAX[0] = "AX";
           regisBX[0] = "BX";
           regisCX[0] = "CX";
           regisDX[0] = "DX";
        for(int x = 1; x < regisAX.length; x++){
           regisAX[x] = "0";
           regisBX[x] = "0";
           regisCX[x] = "0";
           regisDX[x] = "0";
        }
    }
    
    public void viusalizarEstadoRegistro(String [] registro){        
        for(int x = 0; x < registro.length; x++){                          
                System.out.print(registro[x]+" ");  
        }
        System.out.println();
    }
    public void visualizarTodosLosRegistros(){
        String AX = "";
        String BX = "";
        String CX = "";
        String DX = "";
        for(int x = 0; x < regisAX.length; x++){                          
                AX += regisAX[x]+" ";
                BX += regisBX[x]+" ";
                CX += regisCX[x]+" ";
                DX += regisDX[x]+" ";
        }
        System.out.print(AX+"\n"+BX+"\n"+CX+"\n"+DX+"\n");  

    }
    
    public void guardarConConstante(String nomRegistro, int datoAGuardar){        
        String lon = convercionDecimalABinario(datoAGuardar+"");
        if(nomRegistro.equals("AX") && lon.length() <= 16){
            guardarEnRegistrosCompleto(regisAX, lon);
        }else if(nomRegistro.equals("BX") && lon.length() <= 16){
             guardarEnRegistrosCompleto(regisBX, lon);
        }else if(nomRegistro.equals("CX") && lon.length() <= 16){
             guardarEnRegistrosCompleto(regisCX, lon);
        }else if(nomRegistro.equals("DX") && lon.length() <= 16){
             guardarEnRegistrosCompleto(regisDX, lon);
        }else if(nomRegistro.equals("AH") && lon.length() <= 8){
            guardarEnRegistroSuperior(regisAX, lon);
        }else if(nomRegistro.equals("BH") && lon.length() <= 8){
             guardarEnRegistroSuperior(regisBX, lon);
        }else if(nomRegistro.equals("CH") && lon.length() <= 8){
             guardarEnRegistroSuperior(regisCX, lon);
        }else if(nomRegistro.equals("DH") && lon.length() <= 8){
             guardarEnRegistroSuperior(regisDX, lon);
        }else if(nomRegistro.equals("AL") && lon.length() <= 8){
            guardarEnRegistroInferior(regisAX, lon);
        }else if(nomRegistro.equals("BL") && lon.length() <= 8){
             guardarEnRegistroInferior(regisBX, lon);
        }else if(nomRegistro.equals("CL") && lon.length() <= 8){
             guardarEnRegistroInferior(regisCX, lon);
        }else if(nomRegistro.equals("DL") && lon.length() <= 8){
             guardarEnRegistroInferior(regisDX, lon);
        }else{System.out.println("hubo desbordamiento, el número es mayor que el registro");}
    }    
    public void sumarConConstante(String nomRegistro, int datoASumar){
         String lon = convercionDecimalABinario(datoASumar+"");      
         String tmpValor = "";
         String suma = "";
        if(nomRegistro.equals("AX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisAX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-15, tmpValor.length()));    
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisAX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("BX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisBX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-15, tmpValor.length()));    
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisBX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("CX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisCX);         
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-15, tmpValor.length()));            
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisCX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("DX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisDX);                         
           suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-15, tmpValor.length()));                         
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisDX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("AH") && lon.length() <= 8){
            tmpValor = valorDeRegistroSuperior(regisAX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisAX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("BH") && lon.length() <= 8){
            tmpValor = valorDeRegistroSuperior(regisBX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length())); 
            suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisBX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}            
        }else if(nomRegistro.equals("CH") && lon.length() <= 8){
             tmpValor = valorDeRegistroSuperior(regisCX);         
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));   
            suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisCX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("DH") && lon.length() <= 8){
           tmpValor = valorDeRegistroSuperior(regisDX);
           suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length())); 
           suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisDX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("AL") && lon.length() <= 8){
            tmpValor = valorDeRegistroInferior(regisAX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length())); 
            suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisAX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}            
        }else if(nomRegistro.equals("BL") && lon.length() <= 8){
            tmpValor = valorDeRegistroInferior(regisBX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));   
            suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisBX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("CL") && lon.length() <= 8){
             tmpValor = valorDeRegistroInferior(regisCX);         
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));  
            suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisCX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("DL") && lon.length() <= 8){
             tmpValor = valorDeRegistroInferior(regisDX);     
           suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));   
           suma = suma.substring(suma.length()-8, suma.length());
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisDX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }
    }
    public void sumaRegistros(String nomRegUno, String nomRegDos){
        String valorRegUno = valorDeRegistroCompleto(buscaArreRegistro(nomRegUno));
        String valorRegDos = valorDeRegistroCompleto(buscaArreRegistro(nomRegDos));
        String suma = sumaDeBinarios(valorRegUno, valorRegDos);
        suma = suma.substring(suma.length()-16, suma.length());
        if(suma.length() <= 16){
            guardarEnRegistrosCompleto(buscaArreRegistro(nomRegUno), suma);
        }else{System.out.println("El valor de la suma es mayor a lo permitido 16 bit");}
    }
  
    
    private String[] buscaArreRegistro(String nombre){        
        if(nombre.equals("AX")){
            return getRegisAX();
        }else if(nombre.equals("BX")){
            return getRegisBX();
        }else if(nombre.equals("CX")){
            return getRegisCX();
        }else {
            return getRegisDX();
        }        
    }
    
    private void guardarEnRegistroInferior(String[] nomRegistro, String longitud){  
        borrarEnRegistroInferior(nomRegistro);
        for(int x = bitInferiorInicio,n = 0,res = 1; x<= bitInferiorfin; x++){
            try{
                if(longitud.substring(longitud.length()-res,longitud.length()) != null){
               nomRegistro[bitInferiorfin -n] = longitud.substring(longitud.length()-res,longitud.length()-n);
               n++;
               res++;
            }
            }catch(Exception e){}
        }
    }    
    private void guardarEnRegistroSuperior(String[] nomRegistro, String longitud){
        borrarEnRegistroSuperior(nomRegistro);
        for(int x = bitSuperiorInicio ,n =0; x<= bitSuperiorfin; x++){
            try{
                if(longitud.substring(longitud.length()-x, longitud.length()) != null){
                    nomRegistro[bitSuperiorfin-(x-1)] = longitud.substring(longitud.length()-x, longitud.length()-n);
                    n++;
                }    
            }catch(Exception ee){}            
            
        }
    }    
    private void guardarEnRegistrosCompleto(String[] nombreRegistro, String longitud){
        borrarEnRegistrosCompleto(nombreRegistro);
        for(int x = bitInicio,n=0; x<= bitFinal; x++){
            try{
                if(longitud.substring(longitud.length()-x,longitud.length()) != null){
               nombreRegistro[bitFinal-(x-1)] = longitud.substring(longitud.length()-x,longitud.length()-n);
               n++;              
              }
            }catch(Exception e){}
        }
    }

    public void guardarConVariable(String nomRegistro, String variable){
        if(uno.buscarVariable(variable)){
            String lon = uno.obtenVariable(variable); 
            lon = convertirBinarioADecimal(lon);
            int valor = Integer.parseInt(lon);
            guardarConConstante(nomRegistro, valor);
        }else{
            System.out.println("La variable "+variable+" no se encuentra definida");
        }
    }
    public void sumarConVariable(String nomRegistro, String variable){
        if(uno.buscarVariable(variable)){
            String lon = uno.obtenVariable(variable);      
            lon = convertirBinarioADecimal(lon);
            int valor = Integer.parseInt(lon);  
            sumarConConstante(nomRegistro, valor);
        }else{
            System.out.println("La variable "+variable+" no se encuentra definida");
        }
       
    }
    
    private String convertirBinarioADecimal(String numBinario){
        int pos = 0;
        int resultado = 0;
        int[] num = new int[numBinario.length()];   
        for(int x = numBinario.length(); x >0; x--){            
            num[pos] = (int) (Integer.parseInt(numBinario.substring(x-1, x))*(Math.pow(2, pos)));           
            resultado += num[pos];
            pos++;
        }        
        return resultado+"";
    }
    private String valorDeRegistroCompleto(String[] nombreRegistro){
        String numBinario = "";
        for(int x = bitInicio; x<= bitFinal; x++){
            numBinario += nombreRegistro[x];
        }
        return numBinario;
    }
    private String valorDeRegistroSuperior(String[] nomRegistro){
        String numBinario = "";
          for(int x = bitSuperiorInicio; x<= bitSuperiorfin; x++){
            numBinario += nomRegistro[x];
        }   
       return numBinario;   
    }    
    private String valorDeRegistroInferior(String[] nomRegistro){
        String numBinario = "";
        for(int x = bitInferiorInicio; x<= bitInferiorfin; x++){
            numBinario += nomRegistro[x];            
        }        
        return numBinario;
    }
    
     private void borrarEnRegistroInferior(String[] nomRegistro){        
        for(int x = bitInferiorInicio; x<= bitInferiorfin; x++){
           nomRegistro[x] = "0";
        }
    } 
     private void borrarEnRegistroSuperior(String[] nomRegistro){
        for(int x = bitSuperiorInicio; x<= bitSuperiorfin; x++){
                  nomRegistro[x]="0";
        }
    } 
     private void borrarEnRegistrosCompleto(String[] nombreRegistro){
        for(int x = bitInicio,n=0; x<= bitFinal; x++){
            nombreRegistro[x]="0";
        }
    }
    
    
    private String convercionDecimalABinario(String numDecimal){
            String resultado = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numBinario = new String[100];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 2;                
                          decimal = decimal /2;                
                    numBinario[x] = residuo+"";
                    x++;
                                  
              }while(decimal > 0);               
             for(int y = numBinario.length-1; y >= 0; y--){            
                if(!(numBinario[y] == null)){
                  resultado += numBinario[y];
                }
        }     
        return resultado;
    }    
    private String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
    
        return resultado;
    }
    
    public String[] getRegisAX() {
        return regisAX;
    }
    public String[] getRegisBX() {
        return regisBX;
    }
    public String[] getRegisCX() {
        return regisCX;
    }
    public String[] getRegisDX() {
        return regisDX;
    } 
    
     public static void main(String[] args){
        Registros re = new Registros();
       
        System.out.println("Métodos de registros con constantes");
        
        System.out.println("visualizar solo un registtro");
        re.viusalizarEstadoRegistro(re.getRegisAX());
        
        System.out.println("visualizar todos los registro");      
        re.visualizarTodosLosRegistros();
        
        System.out.println("guardar 15 en el registro superior AH"); 
        re.guardarConConstante("AH", 15);
        re.viusalizarEstadoRegistro(re.getRegisAX());
        System.out.println("guardar  255 en el registro inferior BL");  
        re.guardarConConstante("BL", 255);
        re.viusalizarEstadoRegistro(re.getRegisBX());
   
        System.out.println("guardar 35 en el registro CX"); 
        re.guardarConConstante("CX", 35);
        re.viusalizarEstadoRegistro(re.getRegisCX());
         System.out.println("visualizar todos los registros para ver los cambios");         
        re.visualizarTodosLosRegistros();
   
        System.out.println("Sumar 10 en CX");
        re.sumarConConstante("CX", 10);
        re.viusalizarEstadoRegistro(re.getRegisCX());
        
         System.out.println("Sumar 50 en BX");
        re.sumarConConstante("BX", 50);
        re.viusalizarEstadoRegistro(re.getRegisBX());
        
         System.out.println("Sumar 33 en DX");
        re.sumarConConstante("DX", 33);
        re.viusalizarEstadoRegistro(re.getRegisDX());
        
         System.out.println("Guarda 6 en el registro inferios DL");
        re.guardarConConstante("DL",6);
        re.viusalizarEstadoRegistro(re.getRegisDX());
         System.out.println("Sumar 1 en DL");
        re.sumarConConstante("DL",1);
        re.viusalizarEstadoRegistro(re.getRegisDX());
        re.viusalizarEstadoRegistro(re.getRegisAX());
        System.out.println("suma AX mas DX");
        re.sumaRegistros("AX","DX");
        re.viusalizarEstadoRegistro(re.getRegisAX());
        
         System.out.println("Métodos de registros con identificadores en memoria");
         
         TipoByte A = new TipoByte("A", 20);
         TipoByte B = new TipoByte("B", 30);
         TipoByte C = new TipoByte("C", 100);
         TipoByte D = new TipoByte("D", 55);
         
         TipoWord E = new TipoWord("E", 500);
         TipoWord F = new TipoWord("F", 700);
         TipoWord G = new TipoWord("G", 1000);
         TipoWord H = new TipoWord("H", 2000);
         
        
         
         uno.agregarIdentificadorBYTE(A);
         uno.agregarIdentificadorBYTE(B);
         uno.agregarIdentificadorBYTE(C);
         uno.agregarIdentificadorBYTE(D);
         
         uno.agregarIdentificadorWORD(E);
         uno.agregarIdentificadorWORD(F);
         uno.agregarIdentificadorWORD(G);
         uno.agregarIdentificadorWORD(H);
         
         uno.mostrarVariable();
         System.out.println("");
         re.viusalizarEstadoRegistro(re.getRegisAX());
         System.out.println("Guardar variable A en el registro AL");
         re.guardarConVariable("AL","A");
         re.viusalizarEstadoRegistro(re.getRegisAX());
         
         System.out.println("");
         re.viusalizarEstadoRegistro(re.getRegisCX());
         System.out.println("Guardar variable C en el registro CH");
         re.guardarConVariable("CH","C");
         re.viusalizarEstadoRegistro(re.getRegisCX());
         
         System.out.println("");
         re.viusalizarEstadoRegistro(re.getRegisBX());
         System.out.println("Guardar variable E en el registro BX");
         re.guardarConVariable("BX","E");
         re.viusalizarEstadoRegistro(re.getRegisBX());
         
         System.out.println("");
         re.viusalizarEstadoRegistro(re.getRegisDX());
         System.out.println("Guardar variable H en el registro DX");
         re.guardarConVariable("DX","H");
         re.viusalizarEstadoRegistro(re.getRegisDX());
         
          System.out.println("");
          re.viusalizarEstadoRegistro(re.getRegisDX());
          System.out.println("suma la variable G al registro DX");
          re.sumarConVariable("DX","G");
          re.viusalizarEstadoRegistro(re.getRegisDX());
          
          System.out.println("");
          re.viusalizarEstadoRegistro(re.getRegisCX());
          System.out.println("suma la variable B al registro CL");
          re.sumarConVariable("CL","B");
          re.viusalizarEstadoRegistro(re.getRegisCX());
         
    }
   
}
